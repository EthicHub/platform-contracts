/* eslint perfectionist/sort-objects: 'error' */
// @ts-check
import { paroparo } from '@paro-paro/eslint-config'

export default paroparo(
  {
    gitignore: true,
  },
  {
    files: ['**/*.ts', '**/*.vue'],
    languageOptions: {
      globals: {
        $fetch: 'readonly',
        defineNuxtConfig: 'readonly',
        definePageMeta: 'readonly',
        useEvent: 'readonly',
      },
    },
    rules: {
      'antfu/top-level-function': 'off',
      'no-console': 'off',
      'node/prefer-global/buffer': 'off',
      'node/prefer-global/process': 'off',
      'require-await': 'off',
      'sort-exports/sort-exports': 'off',
      'unicorn/catch-error-name': ['error', { name: 'err' }],
      'unicorn/filename-case': 'off',
      'unicorn/prefer-node-protocol': 'off',
      'unused-imports/no-unused-imports': 'off',
    },
  },

  {
    files: ['**/*.vue'],
    rules: {
      'vue/component-name-in-template-casing': [
        'error',
        'PascalCase',
        {
          ignores: ['/^v-/', '/^w3m-/'],
          registeredComponentsOnly: false,
        },
      ],
      'vue/multi-word-component-names': 'off',
      'vue/require-default-prop': 'off',
    },
  },

  {
    files: [
      '**/types/*.ts',
      'abis/index.ts',
      'utils/index.ts',
      'graphql/documents.ts',
      'server/utils/index.ts',
      'composables/index.ts',
      'composables/config/index.ts',
      'composables/dialog/index.ts',
      'composables/form/index.ts',
      'composables/invest/index.ts',
      'composables/lending/index.ts',
      'composables/staking/index.ts',
      'composables/table/index.ts',
      'components/ui/**/index.ts',
    ],
    rules: {
      'sort-exports/sort-exports': 'error',
    },
  },
)
