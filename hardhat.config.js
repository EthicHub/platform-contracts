// require('@openzeppelin/hardhat-upgrades')
// require("@babel/polyfill");
// require("@babel/register");
require("@nomiclabs/hardhat-truffle5");
// require("@nomiclabs/hardhat-solhint");
// require("@nomiclabs/hardhat-etherscan");
// require("@nomiclabs/hardhat-ethers");
// require("@tenderly/hardhat-tenderly");

let secrets = require('./.secrets.json')

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.5.13",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      },
      {
        version: "0.6.12",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      },
      {
        version: "0.7.5",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      },
      {
        version: "0.8.3",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      }
    ]
  },
  networks: {
    local: {
      url: 'http://127.0.0.1:8545',
    },
    chiado: {
      url: secrets.chiado.node_url,
      chainId: 10200,
      accounts: {
        mnemonic: secrets.chiado.mnemonic
      },
      gasPrice: 1000000000, // 1Gwei
    },
    gnosis: {
      url: secrets.gnosis.node_url,
      chainId: 100,
      accounts: secrets.gnosis.pks,
    },
    alfajores: {
      url: secrets.alfajores.node_url,
      chainId: 44787,
      accounts: {
        mnemonic: secrets.alfajores.mnemonic
      },
      gasPrice: 1000000000, // 1Gwei
    },
    celo: {
      url: secrets.celo.node_url,
      chainId: 42220,
      accounts: secrets.celo.pks,
    },
    sepolia: {
      url: secrets.sepolia.node_url,
      chainId: 11155111,
      accounts: {
        mnemonic: secrets.sepolia.mnemonic
      },
      gasPrice: 1000000000, // 1Gwei
    },
    mainnet: {
      url: secrets.mainnet.node_url,
      chainId: 1,
      accounts: secrets.mainnet.pks,
    },
  },
  etherscan: {
    apiKey: secrets.etherscan_api_key
  }
}
