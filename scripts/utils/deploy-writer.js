const fs = require('fs')
var path = require('path')
const root = path.resolve('./')
const DIR = path.join(root, '.deployment')
const { BigNumber } = require('ethers')
const beautify = require('json-beautify')

const needsInit = (x) => {
  return x === null || x === undefined || x === {}
}

module.exports = class DeployResultWriter {
  constructor(network) {
    this._initDir()
    this.network = network
    this.filePath = path.join(DIR, `${this.network}.json`)
    network === 'hardhat' && fs.existsSync(this.filePath) ? this._removeDeploymentFile() : null
    this._loadDeployment()
  }
  _initDir() {
    if (!fs.existsSync(DIR)) {
      fs.mkdirSync(DIR)
    }
  }
  _getMockStableCoin() {
    const mockStableCoin = this.currentDeployment.MockStableCoin
    return mockStableCoin ? mockStableCoin : {}
  }
  _getStorage() {
    const storage = this.currentDeployment.EthicHubStorage
    return storage ? storage : {}
  }
  _getDepositManager() {
    const depositManager = this.currentDeployment.EthicHubDepositManager
    return depositManager ? depositManager : {}
  }
  _init() {
    needsInit(this.currentDeployment.MockStableCoin) ? (this.currentDeployment.MockStableCoin = this._getMockStableCoin()) : null
    needsInit(this.currentDeployment.EthicHubStorage) ? (this.currentDeployment.EthicHubStorage = this._getStorage()) : null
    needsInit(this.currentDeployment.EthicHubDepositManager) ? (this.currentDeployment.EthicHubDepositManager = this._getDepositManager()) : null
  }
  _loadDeployment() {
    if (fs.existsSync(this.filePath, fs.R_OK)) {
      this.currentDeployment = JSON.parse(fs.readFileSync(this.filePath))
    } else {
      this.currentDeployment = {}
    }
    this._init()
    this._saveDeploymentToFile()
  }
  _saveDeploymentToFile() {
    fs.writeFileSync(this.filePath, beautify(this.currentDeployment, null, 2, 80))
  }
  _removeDeploymentFile() {
    fs.unlinkSync(this.filePath);
  }
  addContract(name, address, isProxy) {
    if (!this.currentDeployment[name]) {
      this.currentDeployment[name] = {}
    }
    this.currentDeployment[name].address = address
    this.currentDeployment[name].isProxy = isProxy
    this._saveDeploymentToFile()
  }
  updateContract(name, key, value) {
    if (!this.currentDeployment[name]) {
      throw new Error('contract not deployed: '+ name)
    }
    this.currentDeployment[name][key] = value
    this._saveDeploymentToFile()
  }
}