const { ethers, network, tenderly } = require('hardhat')

const DeployResultWriter = require('../utils/deploy-writer')

async function main() {
  if (['xdai', 'sokol'].includes(network.name)) {
    console.log("Deposit Manager doesn't deploy to xDai or Sokol")
    return
  }
  const writer = new DeployResultWriter(network.name)

  const [owner] = await ethers.getSigners()
  console.log(`Owner address ${owner.address}`)

  const Storage = await ethers.getContractFactory('EthicHubStorage')
  const storage = await Storage.deploy()
  await storage.deployed()

  console.log(`Storage deployed to: ${storage.address}`)
  
  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await tenderly.persistArtifacts({
      name: 'EthicHubStorage',
      address: storage.address,
    })

    await tenderly.push({
      name: 'EthicHubStorage',
      address: storage.address,
    })
  }

  writer.addContract('EthicHubStorage', storage.address, false)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })