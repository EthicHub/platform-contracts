const { ethers, network, tenderly } = require('hardhat')

const DeployResultWriter = require('../utils/deploy-writer')

async function main() {
  const { chainId } = await ethers.provider.getNetwork()

  if (['xdai', 'sokol'].includes(network.name)) {
    console.log("MockStableCoin doesn't deploy to xDai or Sokol")
    return
  }

  const [owner] = await ethers.getSigners()
  console.log(`Owner address ${owner.address}`)
  const MockStableCoin = await ethers.getContractFactory('MockStableCoin')

  const mockStableCoin = await MockStableCoin.deploy(chainId)
  await mockStableCoin.deployed()

  console.log(`MockStableCoin deployed to: ${mockStableCoin.address}`)
  console.log(`Checking balance of owner...`)

  const balance = await mockStableCoin.balanceOf(owner.address)
  console.log(`Balance of ${owner.address} is ${ethers.utils.formatEther(balance.toString())}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await tenderly.persistArtifacts({
      name: 'MockStableCoin',
      address: mockStableCoin.address,
    })

    await tenderly.push({
      name: 'MockStableCoin',
      address: mockStableCoin.address,
    })
  }

  const writer = new DeployResultWriter(network.name)
  writer.addContract('MockStableCoin', mockStableCoin.address, false)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
