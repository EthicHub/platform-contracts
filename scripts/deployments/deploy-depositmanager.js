const { ethers, network, tenderly } = require('hardhat')

const DeployResultWriter = require('../utils/deploy-writer')

async function main() {
  if (['xdai', 'sokol'].includes(network.name)) {
    console.log("Deposit Manager doesn't deploy to xDai or Sokol")
    return
  }
  const writer = new DeployResultWriter(network.name)

  const [owner] = await ethers.getSigners()
  console.log(`Owner address ${owner.address}`)

  const DepositManager = await ethers.getContractFactory('EthicHubDepositManager')

  const depositManager = await DepositManager.deploy()
  await depositManager.deployed()

  console.log(`Deposit Manager deployed to: ${depositManager.address}`)
  
  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await tenderly.persistArtifacts({
      name: 'EthicHubDepositManager',
      address: depositManager.address,
    })

    await tenderly.push({
      name: 'EthicHubDepositManager',
      address: depositManager.address,
    })
  }

  writer.addContract('EthicHubDepositManager', depositManager.address, false)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
