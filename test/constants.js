const { BN } = require('@openzeppelin/test-helpers');
const BN0_05 = new BN(0.5)
const BN1 = new BN(1)

module.exports = { BN0_05, BN1 }