'use strict'
const { ether } = require ('./helpers/ether')
const { advanceBlock } = require('./helpers/advanceToBlock')
const { increaseTimeTo, duration } = require ('./helpers/increaseTime')
const  { latestTime } = require('./helpers/latestTime')
const EVMRevert = require('./helpers/EVMRevert')

const {
    BN
} = require('@openzeppelin/test-helpers')

const Uninitialized = 0
const AcceptingContributions = 1
const Funded = 2
const AwaitingReturn = 3
const ProjectNotFunded = 4
const ContributionReturned = 5
const Default = 6
const LatestVersion = 12

const utils = require("web3-utils")

const chai = require('chai')
chai.use(require('chai-as-promised'))
    .use(require('chai-bn')(BN))
    .should()

const EthicHubLoanRepayment = artifacts.require('EthicHubLoanRepayment')
const MockStorage = artifacts.require('MockStorage')
const MockStableCoin = artifacts.require('MockStableCoin')
const CHAIN_ID = "666"

contract('EthicHubLoanRepayment', function([owner, borrower, investor, investor2, investor3, investor4, localNode, ethicHubTeam, community, arbiter, unknow_person]) {
    beforeEach(async function() {
        await advanceBlock()

        const latestTimeValue = await latestTime()

        this.mockStorage = await MockStorage.new()
        this.mockStableCoin = await MockStableCoin.new(CHAIN_ID)

        this.loanParams = {
            'fundingEndTime': latestTimeValue + duration.days(41),
            'annualInterest': 1500,
            'totalLendingAmount' : ether(3).toString(),
            'lendingDays': 90,
            'ethicHubFee': 300,
            'systemFees': 400,
            'maxDelayDays': 90,
            'stableCoin': this.mockStableCoin.address
        }

        this.actors = {
            'borrower': borrower,
            'localNode': localNode,
            'ethicHubTeam': ethicHubTeam
        }

        this.members = new BN(20)

        await this.mockStorage.setBool(utils.soliditySha3("user", "localNode", localNode), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "representative", borrower), true)

        this.repayment = await EthicHubLoanRepayment.new(
            this.mockStorage.address,
            this.loanParams,
            this.actors
         )

        await this.mockStorage.setAddress(utils.soliditySha3("contract.address", this.repayment.address), this.repayment.address)
        await this.mockStorage.setAddress(utils.soliditySha3("arbiter", this.repayment.address), arbiter)

        await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor2), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor3), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor4), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "community", community), true)
        await this.mockStorage.setBool(utils.soliditySha3("user", "arbiter", arbiter), true)

        // transfer and aprove stableCoin for borrower
        await this.mockStableCoin.transfer(borrower, ether(100000)).should.be.fulfilled;
        await this.mockStableCoin.approve(this.repayment.address, ether(100000), { from: borrower }).should.be.fulfilled;
    })

    describe('initializing', function() {
        it('should not allow to invest before initializing', async function() {
            var someLending = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                this.loanParams,
                this.actors
            )

            await increaseTimeTo(this.loanParams.fundingEndTime - duration.days(0.5))

            var state = await someLending.state()
            state.toNumber().should.be.equal(AwaitingReturn)
        })

        it('should not allow create projects with unregistered local nodes', async function() {
            this.actors.localNode = unknow_person
            await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                this.loanParams,
                this.actors
            ).should.be.rejectedWith(EVMRevert)
        })

        it('should not allow to invest with unregistered representatives', async function() {
            this.actors.borrower = unknow_person
            await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                this.loanParams,
                this.actors
            ).should.be.rejectedWith(EVMRevert)
        })

        it('should be in latest version', async function() {
            let version = await this.repayment.version()
            let expectedVersion = new BN(LatestVersion)
            version.should.be.bignumber.equal(expectedVersion)
        })
    })

    describe('Days calculator', function() {
        it('should calculate correct days', async function() {
            const expectedDaysPassed = 55
            const daysPassed = await this.repayment.getDaysPassedBetweenDates(this.loanParams.fundingEndTime, this.loanParams.fundingEndTime + duration.days(expectedDaysPassed))
            daysPassed.should.be.bignumber.equal(new BN(expectedDaysPassed))
            const sameAsLendingDays = await this.repayment.getDaysPassedBetweenDates(this.loanParams.fundingEndTime, this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays))
            new BN(this.loanParams.lendingDays).should.be.bignumber.equal(sameAsLendingDays)
            const lessThanADay = await this.repayment.getDaysPassedBetweenDates(this.loanParams.fundingEndTime, this.loanParams.fundingEndTime + duration.hours(23))
            new BN(0).should.be.bignumber.equal(lessThanADay)
        })

        it('should fail to operate for time travelers (sorry)', async function() {
            await this.repayment.getDaysPassedBetweenDates(this.loanParams.fundingEndTime, this.loanParams.fundingEndTime - duration.days(2)).should.be.rejectedWith(EVMRevert)
        })
    })

    describe('Partial returning of funds', function() {
        it('full payment of the loan in several transfers should be allowed', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime + duration.days(1))
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount.div(new BN(2)), { from: borrower }).should.be.fulfilled
            await this.repayment.returnBorrowed(borrowerReturnAmount.div(new BN(2)), { from: borrower }).should.be.fulfilled
            const state = await this.repayment.state()
            state.toNumber().should.be.equal(ContributionReturned)
        })

        it('partial payment of the loan should be still default', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime - duration.minutes(1))

            //This should be the edge case : end of funding time + awaiting for return period.
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(10)
            await increaseTimeTo(defaultTime)
            const trueBorrowerReturnAmount = await this.repayment.borrowerReturnAmount() // actual returnAmount
            await this.repayment.returnBorrowed(trueBorrowerReturnAmount.div(new BN(2)), { from : borrower }).should.be.fulfilled
            await this.repayment.returnBorrowed(trueBorrowerReturnAmount.div(new BN(5)), { from : borrower }).should.be.fulfilled

            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(this.loanParams.maxDelayDays + 1)
            await increaseTimeTo(defaultTime)
            await this.repayment.declareProjectDefault({from: owner}).should.be.fulfilled
            var state = await this.repayment.state()
            state.toNumber().should.be.equal(Default)
        })

        it('partial payment of the loan should allow to recover contributions', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime  - duration.minutes(1))

            var investorSendAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(1)).div(new BN(4))
            await this.repayment.setInvestorState(investor, investorSendAmount).should.be.fulfilled
            const investorAfterSendBalance = await this.mockStableCoin.balanceOf(investor)

            var investor2SendAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(3)).div(new BN(4))
            await this.repayment.setInvestorState(investor2, investor2SendAmount).should.be.fulfilled
            const investor2AfterSendBalance = await this.mockStableCoin.balanceOf(investor2)

            //This should be the edge case : end of funding time + awaiting for return period.
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(10)
            await increaseTimeTo(defaultTime)
            const trueBorrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            const notFullAmount = trueBorrowerReturnAmount.div(new BN(4)).mul(new BN(3)) //0.75
            await this.repayment.returnBorrowed(notFullAmount, { from: borrower }).should.be.fulfilled
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(this.loanParams.maxDelayDays + 1)
            await increaseTimeTo(defaultTime)

            await this.repayment.declareProjectDefault({from: owner}).should.be.fulfilled
            var state = await this.repayment.state()
            state.toNumber().should.be.equal(Default)

            await this.repayment.reclaimContributionDefault(investor, {from: investor}).should.be.fulfilled
            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            var expected = new BN(investorAfterSendBalance).add(investorSendAmount.div(new BN(4)).mul(new BN(3)))
            checkLostinTransactions(expected, investorFinalBalance)
            await this.repayment.reclaimContributionDefault(investor2, {from: investor2}).should.be.fulfilled
            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            var expected2 = new BN(investor2AfterSendBalance).add(investor2SendAmount.div(new BN(4)).mul(new BN(3)))
            checkLostinTransactions(expected2, investor2FinalBalance)
            var contractBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractBalance.should.be.bignumber.equal(new BN(0))
        })

        it('partial payment of the loan should not allow to recover interest, local node and team fees', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime - duration.minutes(1))

            var investorSendAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(1)).div(new BN(3))
            await this.repayment.setInvestorState(investor, investorSendAmount).should.be.fulfilled

            var investor2SendAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(2)).div(new BN(3))
            await this.repayment.setInvestorState(investor2, investor2SendAmount).should.be.fulfilled

            //This should be the edge case : end of funding time + awaiting for return period.
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(10)
            await increaseTimeTo(defaultTime)

            const trueBorrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            const notFullAmount = trueBorrowerReturnAmount.div(new BN(4)).mul(new BN(3)) //0.75
            await this.repayment.returnBorrowed(notFullAmount, {from: borrower}).should.be.fulfilled

            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(this.loanParams.maxDelayDays + 1)
            await increaseTimeTo(defaultTime)
            await this.repayment.declareProjectDefault({from: owner}).should.be.fulfilled
            var state = await this.repayment.state()
            state.toNumber().should.be.equal(Default)
            // Reclaims amounts
            await this.repayment.reclaimContributionWithInterest(investor, {from: investor}).should.be.rejectedWith(EVMRevert)

            await this.repayment.reclaimContributionWithInterest(investor2, {from: investor2}).should.be.rejectedWith(EVMRevert)
        })
    })

    describe('Retrieving contributions', function() {
        it('should not allow to retrieve contributions before declaring project contribution returned or default', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime + duration.days(1))
            await this.repayment.setInvestorState(investor, ether(1)).should.be.fulfilled

            var balance = await this.mockStableCoin.balanceOf(this.repayment.address)
            balance.should.be.bignumber.equal(ether(0))

            // can reclaim contribution from everyone
            balance = await this.mockStableCoin.balanceOf(investor)
            await this.repayment.reclaimContributionWithInterest(investor).should.be.rejectedWith(EVMRevert)
        })
    })

    describe('Borrower return', function() {

        it('returning in same date should amount to totalLendingAmount plus fees', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            borrowerReturnAmount.should.be.bignumber.equal(this.loanParams.totalLendingAmount)
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from:borrower}).should.be.fulfilled
            const state = await this.repayment.state()
            state.toNumber().should.be.equal(ContributionReturned)
        })

        it('returning in half total date without fees', async function() {
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : ether(1).toString(),
                'lendingDays': 183,
                'ethicHubFee': 0,
                'systemFees': 0,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let noFeesRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", noFeesRepayment.address), noFeesRepayment.address)
            await increaseTimePastEndingTime(noFeesRepayment, loanParams.lendingDays)
            
            let lendingIncrement = await noFeesRepayment.lendingInterestRatePercentage()
            lendingIncrement.toNumber().should.be.above(10750)
            lendingIncrement.toNumber().should.be.below(10755)
        })

        it('returning in half total date with fees', async function() {
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : ether(1).toString(),
                'lendingDays': 183,
                'ethicHubFee': 400,
                'systemFees': 300,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let feesRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", feesRepayment.address), feesRepayment.address)
            await increaseTimePastEndingTime(feesRepayment, loanParams.lendingDays)
          
            let lendingIncrement = await feesRepayment.lendingInterestRatePercentage()
            lendingIncrement.should.be.bignumber.equal(new BN(11452))
        })


        it('should calculate correct return amount based on return time', async function() {
            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            var state = await this.repayment.state()
            state.toNumber().should.be.equal(AwaitingReturn)

            var interest = parseInt((this.loanParams.annualInterest) * (this.loanParams.lendingDays) / (365))
            var borrowerReturnAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(interest + 10000)).div(new BN(10000))
            var contractBorrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            contractBorrowerReturnAmount.should.be.bignumber.equal(borrowerReturnAmount)

            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(this.loanParams.maxDelayDays)

            await increaseTimeTo(defaultTime)

            interest = parseInt((this.loanParams.annualInterest) * (this.loanParams.lendingDays + this.loanParams.maxDelayDays) / (365))
            borrowerReturnAmount = new BN(this.loanParams.totalLendingAmount).mul(new BN(interest + 10000)).div(new BN(10000))
            contractBorrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            contractBorrowerReturnAmount.should.be.bignumber.equal(borrowerReturnAmount)
        })

        it('should allow the return of proper amount', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime + duration.days(1))
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from:borrower}).should.be.fulfilled
        })
    })

    describe('Default', async function() {
        it('should calculate correct time difference', async function() {
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays)
            for (var delayDays = 0; delayDays <= 10; delayDays++) {
                var resultDays = await this.repayment.getDelayDays(defaultTime + duration.days(delayDays))
                resultDays.toNumber().should.be.equal(delayDays)
            }
        })

        it('should count half a day as full day', async function() {
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays)
            var resultDays = await this.repayment.getDelayDays(defaultTime + duration.days(1.5))
            resultDays.toNumber().should.be.equal(1)
        })

        it('should be 0 days if not yet ended', async function() {
            var defaultTime = this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) - duration.seconds(1)
            var resultDays = await this.repayment.getDelayDays(defaultTime)
            resultDays.toNumber().should.be.equal(0)
        })

        it('should not allow to declare project as default before lending period ends', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime + duration.days(this.loanParams.lendingDays) + duration.days(this.loanParams.maxDelayDays) - duration.days(1))
            await this.repayment.declareProjectDefault().should.be.rejectedWith(EVMRevert)
        })
    })

    describe('Retrieve contribution with interest', async function() {
        it('Should return investors contributions with interests', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investment2 = ether(1)
            const investment3 = ether(0.5)
            const investment4 = ether(1.5)

            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)

            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled
            var state = await this.repayment.state()
            state.toNumber().should.be.equal(AwaitingReturn)

            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled
            const investorInterest = await this.repayment.investorInterest()
            await this.repayment.reclaimContributionWithInterest(investor2, {from: investor2})
            await this.repayment.reclaimContributionWithInterest(investor3, {from: investor3})
            await this.repayment.reclaimContributionWithInterest(investor4, {from: investor4})

            const balance = await this.mockStableCoin.balanceOf(this.repayment.address)
            new BN(balance).toNumber().should.be.below(2)
            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, investment2, investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)
            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, investment3, investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)
            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, investment4, investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)
        })


        it('Should show same returns for investors different time after returned', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investment2 = ether(1)
            const investment3 = ether(0.5)
            const investment4 = ether(1.5)

            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled

            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)

            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            let firstCheck = await this.repayment.checkInvestorReturns(investor2).should.be.fulfilled
            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays + 20)

            let secondCheck = await this.repayment.checkInvestorReturns(investor2).should.be.fulfilled
            firstCheck.should.be.bignumber.equal(secondCheck)
        })

        it('Should not allow to send funds back if not borrower', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investment2 = ether(1)
            const investment3 = ether(0.5)
            const investment4 = ether(1.5)

            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled

            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: investor2}).should.be.rejectedWith(EVMRevert)
        })

        it('Should not allow reclaim twice the funds', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investment2 = ether(1)
            const investment3 = ether(2)

            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled
            
            await this.repayment.reclaimContributionWithInterest(investor2, {from: investor2}).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2, {from: investor2}).should.be.rejectedWith(EVMRevert)
        })

        it('Should not allow returns when contract have balance in other state', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment2 = ether(1)
            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.rejectedWith(EVMRevert)
        })

        it('Should return remainding platform fees if inexact', async function() {
            let lendingAmount = "3539238226800208500"
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : lendingAmount,
                'lendingDays': this.loanParams.lendingDays,
                'ethicHubFee': this.loanParams.ethicHubFee,
                'systemFees': this.loanParams.systemFees,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let realAmountRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", realAmountRepayment.address), realAmountRepayment.address)

            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment  = "1000000000000000000"
            const investment2 = "0261720000000000000"
            const investment3 = "2068378226800210000"
            const investment4 = "0340000000000000000"

            await realAmountRepayment.setInvestorState(investor, investment).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await increaseTimePastEndingTime(realAmountRepayment, this.loanParams.lendingDays)

            await this.mockStableCoin.approve(realAmountRepayment.address, ether(100000), { from: borrower }).should.be.fulfilled;
            await realAmountRepayment.returnBorrowed("220056000000000", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("188440380000000000", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("8657779357692697862", {from: borrower}).should.be.fulfilled
            
            await realAmountRepayment.reclaimContributionWithInterest(investor3, {from: investor3})
            await realAmountRepayment.reclaimContributionWithInterest(investor, {from: investor})
            await realAmountRepayment.reclaimContributionWithInterest(investor2, {from: investor2})
        })

        it('should be interest 0% if the project is repaid on the same day', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investment2 = ether(1)
            const investment3 = ether(0.5)
            const investment4 = ether(1.5)

            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)

            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled

            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()

            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            // Get the contribution 3 years later
            await increaseTimeTo(this.loanParams.fundingEndTime + duration.days(109500))
            // borrowerReturnDays = 0 and interest = 10000
            const borrowerReturnDays = await this.repayment.borrowerReturnDays()
            borrowerReturnDays.toNumber().should.be.equal(0)
            const investorInterest = await this.repayment.investorInterest()
            investorInterest.toNumber().should.be.equal(10000)
            await this.repayment.reclaimContributionWithInterest(investor2, {from: investor2})
            await this.repayment.reclaimContributionWithInterest(investor3, {from: investor3})
            await this.repayment.reclaimContributionWithInterest(investor4, {from: investor4})

            const balance = await this.mockStableCoin.balanceOf(this.repayment.address)
            new BN(balance).toNumber().should.be.below(2)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, investment2, investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, investment3, investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)

            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, investment4, investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)
        })

    })

    describe('Reclaim leftover eth', async function() {
        it('should send leftover dai to team if its correct state, all parties have reclaimed theirs', async function() {
            let lendingAmount = "353923822680020850"
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : lendingAmount,
                'lendingDays': this.loanParams.lendingDays,
                'ethicHubFee': this.loanParams.ethicHubFee,
                'systemFees': this.loanParams.systemFees,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let realAmountRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", realAmountRepayment.address), realAmountRepayment.address)
            await this.mockStorage.setAddress(utils.soliditySha3("arbiter", realAmountRepayment.address), arbiter)

            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment  = "100000000000000000"
            const investment2 = "026172000000000000"
            const investment3 = "206837822680021000"
            const investment4 = "034000000000000000"

            await realAmountRepayment.setInvestorState(investor, investment).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await increaseTimePastEndingTime(realAmountRepayment, this.loanParams.lendingDays)
            await this.mockStableCoin.approve(realAmountRepayment.address, ether(100000), { from: borrower }).should.be.fulfilled;
            await realAmountRepayment.returnBorrowed("22005600000002", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("18844038000000000", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("865777935769269786", {from: borrower}).should.be.fulfilled

            await realAmountRepayment.reclaimContributionWithInterest(investor3, {from: investor3}).should.be.fulfilled
            await realAmountRepayment.reclaimContributionWithInterest(investor, {from: investor}).should.be.fulfilled
            await realAmountRepayment.reclaimContributionWithInterest(investor2, {from: investor2}).should.be.fulfilled
            const teamBalance = await this.mockStableCoin.balanceOf(ethicHubTeam)
            await realAmountRepayment.reclaimLeftover({from: arbiter}).should.be.fulfilled

            const newBalance = await this.mockStableCoin.balanceOf(ethicHubTeam)
            newBalance.should.be.bignumber.above(teamBalance)
        })

        it('should fail to send leftover dai to team if its correct state, without all contributors reclaimed', async function() {
            let lendingAmount = "353923822680020850"
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : lendingAmount,
                'lendingDays': this.loanParams.lendingDays,
                'ethicHubFee': this.loanParams.ethicHubFee,
                'systemFees': this.loanParams.systemFees,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let realAmountRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", realAmountRepayment.address), realAmountRepayment.address)
            await this.mockStorage.setAddress(utils.soliditySha3("arbiter", realAmountRepayment.address), arbiter)

            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment  = "100000000000000000"
            const investment2 = "026172000000000000"
            const investment3 = "206837822680021000"
            const investment4 = "034000000000000000"

            await realAmountRepayment.setInvestorState(investor, investment).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await increaseTimePastEndingTime(realAmountRepayment, this.loanParams.lendingDays)
            await this.mockStableCoin.approve(realAmountRepayment.address, ether(100000), { from: borrower }).should.be.fulfilled;
            await realAmountRepayment.returnBorrowed("22005600000002", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("18844038000000000", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("865777935769269786", {from: borrower}).should.be.fulfilled

            await realAmountRepayment.reclaimContributionWithInterest(investor3, {from: investor3}).should.be.fulfilled
            await realAmountRepayment.reclaimContributionWithInterest(investor, {from: investor}).should.be.fulfilled
            await realAmountRepayment.reclaimLeftover({from: arbiter}).should.be.rejectedWith(EVMRevert)
        })
        it('should fail to send leftover dai to team if its correct state if not arbiter', async function() {
            let lendingAmount = "353923822680020850"
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : lendingAmount,
                'lendingDays': this.loanParams.lendingDays,
                'ethicHubFee': this.loanParams.ethicHubFee,
                'systemFees': this.loanParams.systemFees,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let realAmountRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", realAmountRepayment.address), realAmountRepayment.address)
            await this.mockStorage.setAddress(utils.soliditySha3("arbiter", realAmountRepayment.address), arbiter)

            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment  = "100000000000000000"
            const investment2 = "026172000000000000"
            const investment3 = "206837822680021000"
            const investment4 = "034000000000000000"

            await realAmountRepayment.setInvestorState(investor, investment).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await increaseTimePastEndingTime(realAmountRepayment, this.loanParams.lendingDays)
            await this.mockStableCoin.approve(realAmountRepayment.address, ether(100000), { from: borrower }).should.be.fulfilled;
            await realAmountRepayment.returnBorrowed("22005600000002", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("18844038000000000", {from: borrower}).should.be.fulfilled
            await realAmountRepayment.returnBorrowed("865777935769269786", {from: borrower}).should.be.fulfilled

            await realAmountRepayment.reclaimContributionWithInterest(investor3, {from: investor3}).should.be.fulfilled
            await realAmountRepayment.reclaimContributionWithInterest(investor, {from: investor}).should.be.fulfilled
            await realAmountRepayment.reclaimContributionWithInterest(investor2, {from: investor2}).should.be.fulfilled
            await realAmountRepayment.reclaimLeftover({from: investor}).should.be.rejectedWith(EVMRevert)
        })

        it('should fail to send leftover dai to team if not correct state', async function() {
            let lendingAmount = "353923822680020850"
            let loanParams = {
                'fundingEndTime': this.loanParams.fundingEndTime,
                'annualInterest': this.loanParams.annualInterest,
                'totalLendingAmount' : lendingAmount,
                'lendingDays': this.loanParams.lendingDays,
                'ethicHubFee': this.loanParams.ethicHubFee,
                'systemFees': this.loanParams.systemFees,
                'maxDelayDays': this.loanParams.maxDelayDays,
                'stableCoin': this.mockStableCoin.address
            }

            let actors = {
                'borrower': borrower,
                'localNode': localNode,
                'ethicHubTeam': ethicHubTeam
            }

            let realAmountRepayment = await EthicHubLoanRepayment.new(
                this.mockStorage.address,
                loanParams,
                actors
            ).should.be.fulfilled

            await this.mockStorage.setAddress(utils.soliditySha3("contract.address", realAmountRepayment.address), realAmountRepayment.address)
            await this.mockStorage.setAddress(utils.soliditySha3("arbiter", realAmountRepayment.address), arbiter)

            await increaseTimeTo(this.loanParams.fundingEndTime)
            const investment  = "100000000000000000"
            const investment2 = "026172000000000000"
            const investment3 = "206837822680021000"
            const investment4 = "034000000000000000"

            await realAmountRepayment.setInvestorState(investor, investment).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await realAmountRepayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await realAmountRepayment.reclaimLeftover({from: arbiter}).should.be.rejectedWith(EVMRevert)
        })
    })

    describe('Send partial return', async function() {
        it('Should allow to reclaim partial return from contributor', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInvestment = ether(1)
            const investor2Investment = ether(2)
            await this.repayment.setInvestorState(investor, investorInvestment).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, investor2Investment).should.be.fulfilled

            var investorInitialBalance = await this.mockStableCoin.balanceOf(investor)

            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount()

            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            var investorInitialBalance = await this.mockStableCoin.balanceOf(investor)

            const investorInterest = await this.repayment.investorInterest()

            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled

            let reclaimStatus = await this.repayment.getUserContributionReclaimStatus(investor)
            reclaimStatus.should.be.equal(true)
            reclaimStatus = await this.repayment.getUserContributionReclaimStatus(investor2)
            reclaimStatus.should.be.equal(true)

            var investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            var expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, investorInvestment.sub(ether(1).div(new BN(3))), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            var investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            var expectedInvestor2Balance = getExpectedInvestorBalance(investorInitialBalance, investor2Investment.sub(ether(2).div(new BN(3))), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)
        })
    })

    describe('Change borrower', async function() {

        it('Should allow to change borrower with registered arbiter', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.mockStorage.setBool(utils.soliditySha3("user", "representative", investor3), true)
            await this.repayment.setBorrower(investor3, {from: arbiter}).should.be.fulfilled
            let borrower = await this.repayment.borrower()
            borrower.should.be.equal(investor3)
        })

        it('Should not allow to change borrower with unregistered arbiter', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.repayment.setBorrower(investor3, {from: owner}).should.be.rejectedWith(EVMRevert)
        })

    })

    describe('Change investor', async function() {

        it('Should allow to change investor with registered arbiter', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor), true)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor2), true)
            await this.repayment.setInvestorState(investor, ether(1)).should.be.fulfilled
            await this.repayment.changeInvestorAddress(investor, investor2, {from: arbiter}).should.be.fulfilled

            var contributionAmount = await this.repayment.checkInvestorContribution(investor2)
            contributionAmount.should.be.bignumber.equal(ether(1))
            contributionAmount = await this.repayment.checkInvestorContribution(investor)
            contributionAmount.should.be.bignumber.equal(new BN(0))
        })

        it('Should not allow to change investor to unregistered investor', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor), true)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor2), false)
            await this.repayment.setInvestorState(investor, ether(1)).should.be.fulfilled
            await this.repayment.changeInvestorAddress(investor, investor2, {from: arbiter}).should.be.rejectedWith(EVMRevert)
        })

        it('Should not allow to change new investor who have already invested', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor), true)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor2), true)
            await this.repayment.setInvestorState(investor, ether(1)).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, ether(1)).should.be.fulfilled
            await this.repayment.changeInvestorAddress(investor, investor2, {from: arbiter}).should.be.rejectedWith(EVMRevert)
       })


        it('Should not allow to change borrower with unregistered arbiter', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor), true)
            await this.mockStorage.setBool(utils.soliditySha3("user", "investor", investor2), true)
            await this.repayment.changeInvestorAddress(investor, investor2, {from: owner}).should.be.rejectedWith(EVMRevert)
        })
    })

    describe('set investor state', async function() {
        it('Set investors one to one without excess', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInitialBalance = await this.mockStableCoin.balanceOf(investor)
            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))

            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            const investment3 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))
            const investment4 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))

            await this.repayment.setInvestorState(investor, investment).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled
            
            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount();
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled
            
            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor3).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor4).should.be.fulfilled
            
            const investorInterest = await this.repayment.investorInterest()
            
            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            const expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(2)), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(4)), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)

            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)

            const contractFinalBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractFinalBalance.should.be.bignumber.equal(new BN(0))
        })

        it('Set investors one to one with excess', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInitialBalance = await this.mockStableCoin.balanceOf(investor)
            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))


            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            const investment3 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))
            // excess amount set state
            const investment4 = new BN(this.loanParams.totalLendingAmount)

            await this.repayment.setInvestorState(investor, investment).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)

            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount();
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor3).should.be.fulfilled

            const investorInterest = await this.repayment.investorInterest()

            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            const expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(2)), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(4)), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)
        })

        it('Set investors one to one with excess and change this excess', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInitialBalance = await this.mockStableCoin.balanceOf(investor)
            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))


            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            const investment3 = new BN(this.loanParams.totalLendingAmount).div(new BN(6))
            const investment4 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))

            await this.repayment.setInvestorState(investor, investment).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.rejectedWith(EVMRevert)
            await this.repayment.changeInvestorState(investor3, new BN(this.loanParams.totalLendingAmount).div(new BN(8))).should.be.fulfilled
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled


            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount();
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor3).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor4).should.be.fulfilled

            const investorInterest = await this.repayment.investorInterest()

            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            const expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(2)), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(4)), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)

            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)

            const contractFinalBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractFinalBalance.should.be.bignumber.equal(new BN(0))
        })

        it('Set investors one to one with excess and should not change the investment if not set yet', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInitialBalance = await this.mockStableCoin.balanceOf(investor)
            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))


            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            // excess amount set state
            const investment3 = new BN(this.loanParams.totalLendingAmount)
            const investment4 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))

            await this.repayment.setInvestorState(investor, investment).should.be.fulfilled
            await this.repayment.setInvestorState(investor2, investment2).should.be.fulfilled
            await this.repayment.setInvestorState(investor3, investment3).should.be.rejectedWith(EVMRevert)
            await this.repayment.setInvestorState(investor4, investment4).should.be.fulfilled
            await this.repayment.changeInvestorState(investor3, new BN(this.loanParams.totalLendingAmount).div(new BN(8))).should.be.rejectedWith(EVMRevert)
            await this.repayment.setInvestorState(investor3, new BN(this.loanParams.totalLendingAmount).div(new BN(8))).should.be.fulfilled


            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount();
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor3).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor4).should.be.fulfilled

            const investorInterest = await this.repayment.investorInterest()

            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            const expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(2)), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(4)), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)

            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)

            const contractFinalBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractFinalBalance.should.be.bignumber.equal(new BN(0))
        })

        it('Set investors in array', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)

            const investorInitialBalance = await this.mockStableCoin.balanceOf(investor)
            const investor2InitialBalance = await this.mockStableCoin.balanceOf(investor2)
            const investor3InitialBalance = await this.mockStableCoin.balanceOf(investor3)
            const investor4InitialBalance = await this.mockStableCoin.balanceOf(investor4)
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))

            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            const investment3 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))
            const investment4 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))

            const addresses = [investor, investor2, investor3, investor4]
            const amounts = [investment, investment2, investment3, investment4]

            await this.repayment.setInvestorsStates(addresses, amounts).should.be.fulfilled

            await increaseTimePastEndingTime(this.repayment, this.loanParams.lendingDays)
            const borrowerReturnAmount = await this.repayment.borrowerReturnAmount();
            await this.repayment.returnBorrowed(borrowerReturnAmount, {from: borrower}).should.be.fulfilled

            await this.repayment.reclaimContributionWithInterest(investor).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor2).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor3).should.be.fulfilled
            await this.repayment.reclaimContributionWithInterest(investor4).should.be.fulfilled

            const investorInterest = await this.repayment.investorInterest()

            const investorFinalBalance = await this.mockStableCoin.balanceOf(investor)
            const expectedInvestorBalance = getExpectedInvestorBalance(investorInitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(2)), investorInterest)
            checkLostinTransactions(expectedInvestorBalance, investorFinalBalance)

            const investor2FinalBalance = await this.mockStableCoin.balanceOf(investor2)
            const expectedInvestor2Balance = getExpectedInvestorBalance(investor2InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(4)), investorInterest)
            checkLostinTransactions(expectedInvestor2Balance, investor2FinalBalance)

            const investor3FinalBalance = await this.mockStableCoin.balanceOf(investor3)
            const expectedInvestor3Balance = getExpectedInvestorBalance(investor3InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor3Balance, investor3FinalBalance)

            const investor4FinalBalance = await this.mockStableCoin.balanceOf(investor4)
            const expectedInvestor4Balance = getExpectedInvestorBalance(investor4InitialBalance, new BN(this.loanParams.totalLendingAmount).div(new BN(8)), investorInterest)
            checkLostinTransactions(expectedInvestor4Balance, investor4FinalBalance)

            const contractFinalBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractFinalBalance.should.be.bignumber.equal(new BN(0))
        })

        it('Set investors and amounts with distinct arrays length', async function() {
            await increaseTimeTo(this.loanParams.fundingEndTime)
            
            const contractInitialBalance = await this.mockStableCoin.balanceOf(this.repayment.address)
            contractInitialBalance.should.be.bignumber.equal(new BN(0))

            const investment = new BN(this.loanParams.totalLendingAmount).div(new BN(2))
            const investment2 = new BN(this.loanParams.totalLendingAmount).div(new BN(4))
            const investment3 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))
            const investment4 = new BN(this.loanParams.totalLendingAmount).div(new BN(8))

            const addresses = [investor, investor2, investor3]
            const amounts = [investment, investment2, investment3, investment4]

            await this.repayment.setInvestorsStates(addresses, amounts).should.be.rejectedWith(EVMRevert)
        })

    })

    async function increaseTimePastEndingTime(lendingContract, increaseDays) {
        const fundingEnd = await lendingContract.fundingEndTime()
        const returnDate = fundingEnd.add(new BN(duration.days(increaseDays)))
        await increaseTimeTo(returnDate)
    }


    function getExpectedInvestorBalance(initialAmount, contribution, interest) {
        const contributionBN = new BN(contribution)
        const received = contributionBN.mul(interest).div(new BN(10000))
        const initialAmountBN = new BN(initialAmount)
        return initialAmountBN.sub(new BN(contribution)).add(received)
    }

    function checkLostinTransactions(expected, actual) {
        const expectedBN = new BN(expected)
        const lost = expectedBN.sub(new BN(actual))
        // /* Should be below 0.02 eth */
        lost.should.be.bignumber.below('20000000000000000')
    }
})