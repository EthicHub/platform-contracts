const advanceBlock = async () => {
  // First we increase the time
  return new Promise((resolve, reject) => {
    web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [Date.now()],
        id: 0,
    }, err1 => {
        if (err1) return reject(err1)

        web3.currentProvider.send({
            jsonrpc: '2.0',
            method: 'evm_mine',
            id: 0,
        }, (err2, res) => {
            return err2 ? reject(err2) : resolve(res)
        })
    })
  })
}

// Advances the block number so that the last mined block is `number`.
const advanceToBlock = async(number) => {
    // First we increase the time
  return new Promise((resolve, reject) => {
    web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [number],
        id: 0,
      }, err1 => {
        if (err1) return reject(err1)

        web3.currentProvider.send({
            jsonrpc: '2.0',
            method: 'evm_mine',
            id: 0,
        }, (err2, res) => {
            return err2 ? reject(err2) : resolve(res)
        })
    })
  })
}

module.exports = { advanceBlock, advanceToBlock }