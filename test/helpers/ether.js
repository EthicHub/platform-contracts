const {
    BN
} = require('@openzeppelin/test-helpers');
const utils = require("web3-utils");

const ether = (n) => {
    return new BN(utils.toWei(n.toString(), 'ether'));
}

module.exports = { ether }