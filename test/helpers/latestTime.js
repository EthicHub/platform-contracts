// Returns the time of the last mined block in seconds
const latestTime = async () => {
    let latestBlockNumber = await web3.eth.getBlockNumber()
    let latestBlock = await web3.eth.getBlock(latestBlockNumber)
    return latestBlock.timestamp
}

module.exports = { latestTime }